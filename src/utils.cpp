#include <fstream>
#include <sstream>
#include <string>
#include "utils.hpp"

Mat read_csv(const string &filename) {
    fstream fin;
    fin.open(filename, ios::in);
    vector<double> values;
    string line;
    int row=0, column=0;
    while (getline(fin, line)) {
        istringstream sin(line);
        string value;
        column = 0;
        while (getline(sin, value, ',')) {
            values.push_back(stod(value));
            column++;
        }
        row++;
    }
    fin.close();
    if (row==0 || column==0)
        return Mat(3, 1);
    return Mat(row, column, values);
}

map<string, double> readKeyValuePairs(const string& fileName) {
    map<string, double> myMap;
    ifstream file(fileName);
    string line;
    while (getline(file, line)) {
        stringstream ss(line);
        string key;
        double value;
        getline(ss, key, ',');
        ss >> value;
        myMap[key] = value;
    }
    return myMap;
}
