#ifndef UTILS_H
#define UTILS_H
#include <map>
#include <ctime>
#include <random>
#include "simucpp.hpp"

#define MIN(a,b)                 ((a)<(b)?(a):(b))
#define MAX(a,b)                 ((a)>(b)?(a):(b))

using namespace zhnmat;
using namespace std;


constexpr double LIMIT(double x, double min, double max) {
    return (x)<=(min) ? (min) : ((x)>=(max) ? (max) : (x));}

// 从文件中读取数据
Mat read_csv(const string &filename);
// 从文件中读取键值对
map<string, double> readKeyValuePairs(const string& fileName);

#endif // UTILS_H
