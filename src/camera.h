#ifndef CAMERA_H
#define CAMERA_H
#include "raylib.h"

#if defined(__cplusplus)
extern "C" {
#endif

void Init_Camera(Camera *camera);
void Update_Camera(Camera *camera);
void SetReferenceFrame(Matrix frame);

#if defined(__cplusplus)
}
#endif

#endif // CAMERA_H
